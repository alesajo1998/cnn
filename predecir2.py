import numpy as np
import tensorflow as tf 
from keras.preprocessing.image import load_img, img_to_array
from keras.models import load_model

longitud, altura = 200, 200
modelo = 'modelo.h5'
pesos_modelo ='pesos.h5'
#cnn = load_model(modelo)
cnn = tf.keras.models.load_model(modelo)
cnn.load_weights(pesos_modelo)

def predict(file):
  x = load_img(file, target_size=(longitud, altura))
  x = img_to_array(x)
  x = np.expand_dims(x, axis=0)
  array = cnn.predict(x)
  result = array[0]
  answer = np.argmax(result)
  if answer == 0:
    print("pred: Derecha")
  elif answer == 1:
    print("pred: Pare")
  elif answer == 2:
    print("pred: Resalto")
  return answer
imagen1="1.jpg" #Routes
imagen2="2.jpg"
imagen3="3.jpg"
predict(imagen1)
predict(imagen2)
predict(imagen3)




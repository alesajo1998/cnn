import numpy as np
import cv2
import tensorflow as tf 
from keras.preprocessing.image import load_img, img_to_array
from keras.models import load_model
import time

modelo = 'modelo.h5' #Route
pesos_modelo ='pesos.h5' #Route
cnn = tf.keras.models.load_model(modelo)
cnn.load_weights(pesos_modelo)

def predict(x): 
  x = img_to_array(x)
  x = np.expand_dims(x, axis=0)
  array = cnn.predict(x)
  result = array[0]
  answer = np.argmax(result)
  if answer == 0:
    print("pred: Derecha")
  elif answer == 2:
    print("pred: Pare")
  elif answer == 3:
    print("pred: Resalto")  
  return answer

cap = cv2.VideoCapture(0)
while(cap.isOpened()):
   ret, image = cap.read()
   cv2.imshow('frame',image)
   img= cv2.resize(image  , (200 , 200))
   predict(img)
   time.sleep(1)
   if cv2.waitKey(1) & 0xFF == ord('q'):
    	break

cap.release()
cv2.destroyAllWindows() 








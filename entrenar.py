import sys
import os
import tensorflow as tf
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras import optimizers
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dropout, Flatten, Dense, Activation
from tensorflow.python.keras.layers import  Convolution2D, MaxPooling2D
from tensorflow.python.keras import backend as K

K.clear_session()
tf.compat.v1.disable_eager_execution()

data_entrenamiento = 'data\\entrenamiento' #Route
data_validacion = 'data\\validacion' #Route

#Parameters

epocas=20
longitud, altura = 200, 200
batch_size = 32 #Numero de imagenes que se procesa en cada paso
pasos = 1000 #Numero de veces que se va a procesar la informacion en cada epoca
validation_steps = 300 #Numero de veces que se van a prosesar después de los pasos.
filtrosConv1 = 32 #Numero de filtros en la convulución 1. Profundidad 32. 
filtrosConv2 = 64
tamano_filtro1 = (3, 3) #Tamaño filtro 1 3px por 3px(altura y longitud.) 
tamano_filtro2 = (2, 2)
tamano_pool = (2, 2) #Tamaño de filtro en maxpooling 
clases = 4 #Numero de clases
lr = 0.0004 #learning rate. que tan grandes van a ser los ajustes que hace nuestra red neuronal para acercarse a una solución óptima. 

##Preparamos nuestras imagenes

entrenamiento_datagen = ImageDataGenerator(
    rescale=1. / 255, #Cada uno de nuestros pixeles se reescalan de 0 a 255 a 0 a 1. 
    shear_range=0.1,#Generar nuestras imágenes inclinadas para que aprenda también
    zoom_range=0.3, #Le hace zoom para que aprenda a distinguir las imagenes con zoom. 
    horizontal_flip=True #La voltea para que aprenda las imágenes al revés. 
    )

test_datagen = ImageDataGenerator(rescale=1. / 255) #Para la validación la imagen normal.

entrenamiento_generador = entrenamiento_datagen.flow_from_directory(
    data_entrenamiento,
    target_size=(altura, longitud),
    batch_size=batch_size, 
    class_mode='categorical') 
#Va a entrar a data, va a brir las carpetas y procesa las imagenes que encuentre. 
#Va a hacer una clsificación categorica (por clases).

validacion_generador = test_datagen.flow_from_directory(
    data_validacion,
    target_size=(altura, longitud),
    batch_size=batch_size,
    class_mode='categorical')
#Set de validación.

cnn = Sequential()  #Red neuronal secuencial.Varias capas apiladas entre ellas.
cnn.add(Convolution2D(filtrosConv1, tamano_filtro1, padding ="same", input_shape=(longitud, altura, 3), activation='relu'))
#Primera capa. En 2D. Con el número de filtros que definimos anteriormente. padding "same", función de activación relu.
cnn.add(MaxPooling2D(pool_size=tamano_pool))
#Capa de pooling.
cnn.add(Convolution2D(filtrosConv2, tamano_filtro2, padding ="same", activation="relu"))
#Segunda capa.
cnn.add(MaxPooling2D(pool_size=tamano_pool))

cnn.add(Flatten()) #Hacer plana la imagen
cnn.add(Dense(256, activation='relu')) #256 neuronas en la siguiente capa. 
cnn.add(Dropout(0.3)) #Durante el entrenamiento se apagan el 50% para que no aprenda un camino fijo sino que aprenda caminos alternos.
cnn.add(Dense(clases, activation='softmax')) #Ultima capa.Softmax es para clasificación, el valor que tenga el porcentaje más alto es la clasificación correcto


cnn.compile(loss='categorical_crossentropy',
            optimizer=optimizers.Adam(lr=lr),
            metrics=['accuracy'])
#Durante el entranamiento su función de perdida muestre que tan bien o que tan mal va.
cnn.fit_generator(
    entrenamiento_generador,
    steps_per_epoch=pasos,
    epochs=epocas,
    validation_data=validacion_generador,
    validation_steps=validation_steps)

target_dir='model'#Route
if not os.path.exists(target_dir):
  os.mkdir(target_dir)
cnn.save('modelo.h5') #Route
cnn.save_weights('pesos.h5') #Route
